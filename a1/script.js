console.log('Hi! B248.');

const [txtFirstNameEl, txtLastNameEl, spanFullNameEl] = [
    document.querySelector("#txt-first-name"),
    document.querySelector("#txt-last-name"),
    document.querySelector("#span-full-name")
];

function updateFullName() {
    spanFullNameEl.innerHTML = `${txtFirstNameEl.value} ${txtLastNameEl.value}`;
}

txtFirstNameEl.addEventListener('input', updateFullName);
txtLastNameEl.addEventListener('input', updateFullName);
